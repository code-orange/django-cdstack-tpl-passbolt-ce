
user www-data;
worker_processes auto;

error_log /var/log/nginx/error.log warn;
pid /run/nginx.pid;

include /etc/nginx/modules-enabled/*.conf;

events {
    worker_connections 1024;
}

http {
    include /etc/nginx/mime.types;
    default_type application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log /var/log/nginx/access.log main;

    server_tokens off;

    ssl_early_data on;
    ssl_protocols TLSv1.2 TLSv1.3;
    ssl_prefer_server_ciphers on; 
    ssl_dhparam /etc/nginx/dhparam.pem;
    ssl_ciphers EECDH+AESGCM:EDH+AESGCM;
    ssl_ecdh_curve secp384r1;
    ssl_session_timeout  10m;
    ssl_session_cache shared:SSL:10m;
    ssl_session_tickets off;

    ssl_stapling on;
    ssl_stapling_verify on;

    resolver {{ network_public_dns_server1 }} {{ network_public_dns_server2 }} valid=300s;
    resolver_timeout 5s;

    map $upstream_http_strict_transport_security $custom_strict_transport_security {
        "~."          $upstream_http_strict_transport_security;
        default       "max-age=63072000; includeSubDomains; preload";
    }

    proxy_hide_header Strict-Transport-Security;
    add_header Strict-Transport-Security $custom_strict_transport_security;

    map $upstream_http_content_security_policy $custom_content_security_policy {
        "~."          $upstream_http_content_security_policy;
        default       "upgrade-insecure-requests";
    }

    proxy_hide_header Content-Security-Policy;
    add_header Content-Security-Policy $custom_content_security_policy;

    map $upstream_http_x_frame_options $custom_x_frame_options {
        "~."          $upstream_http_x_frame_options;
        default       "sameorigin";
    }

    proxy_hide_header X-Frame-Options;
    add_header X-Frame-Options $custom_x_frame_options;

    map $upstream_http_x_content_type_options $custom_x_content_type_options {
        "~."          $upstream_http_x_content_type_options;
        default       "nosniff";
    }

    proxy_hide_header X-Content-Type-Options;
    add_header X-Content-Type-Options $custom_x_content_type_options;

    map $upstream_http_x_xss_protection $custom_x_xss_protection {
        "~."          $upstream_http_x_xss_protection;
        default       "1; mode=block";
    }

    proxy_hide_header X-XSS-Protection;
    add_header X-XSS-Protection $custom_x_xss_protection;

    map $upstream_http_cache_control $custom_cache_control {
        "~."          $upstream_http_cache_control;
        default       "no-cache";
    }

    proxy_hide_header Cache-Control;
    add_header Cache-Control $custom_cache_control;

    map $http_upgrade $connection_upgrade {
        default       upgrade;
        ''            close;
    }

    sendfile on;
    tcp_nopush on;
    tcp_nodelay on;

    # gzip
    gzip on;
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_types text/plain text/css text/xml application/json application/javascript application/xml+rss application/atom+xml image/svg+xml;

    # brotli
    brotli on;
    brotli_comp_level 6;
    brotli_types text/xml image/svg+xml application/x-font-ttf image/vnd.microsoft.icon application/x-font-opentype application/json font/eot application/vnd.ms-fontobject application/javascript font/otf application/xml application/xhtml+xml text/javascript  application/x-javascript text/plain application/x-font-truetype application/xml+rss image/x-icon font/opentype text/css image/x-win-bitmap;

    server_names_hash_bucket_size 128;
    server_names_hash_max_size 8192;

    keepalive_timeout 65;
    types_hash_max_size 2048;
    client_max_body_size 100M;
    client_body_buffer_size 1M;

    proxy_max_temp_file_size 0;

    set_real_ip_from 192.168.0.0/16;
    set_real_ip_from 172.16.0.0/12;
    set_real_ip_from 10.0.0.0/8;


    include /etc/nginx/conf.d/*.conf;
    include /etc/nginx/sites-enabled/*;
}
